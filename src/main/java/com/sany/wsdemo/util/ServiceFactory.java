package com.sany.wsdemo.util;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

/**
 * <p></p>
 *
 * @author jiuhua.xu
 * @version 1.0
 * @since JDK 1.8
 */
public class ServiceFactory {

    public static<T> T getService(Class<T> clazz) {
        T t = null;
        try {
            // 接口地址
            String address = "http://127.0.0.1:8811/soap/user?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(clazz);
            // 创建一个代理接口实现
            t = (T) jaxWsProxyFactoryBean.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }




}
